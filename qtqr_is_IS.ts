<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="is" sourcelanguage="">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="qtqr.py" line="35"/>
        <source>QtQR: QR Code Generator</source>
        <translation>QtQR: QR-kóðari</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="46"/>
        <source>Text</source>
        <translation>Texti</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="47"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="48"/>
        <source>Bookmark</source>
        <translation>Bókmerki</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="49"/>
        <source>E-Mail</source>
        <translation>Tölvupóstfang</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="50"/>
        <source>Telephone Number</source>
        <translation>Símanúmer</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="51"/>
        <source>Contact Information (PhoneBook)</source>
        <translation>Tengiliðaskrá (Símaskrá)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="52"/>
        <source>SMS</source>
        <translation>SMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="53"/>
        <source>MMS</source>
        <translation>MMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="54"/>
        <source>Geolocalization</source>
        <translation>Hnattstaðsetning</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="99"/>
        <source>Text to be encoded:</source>
        <translation>Texti sem á að kóða:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="103"/>
        <source>URL to be encoded:</source>
        <translation>Slóð/URL sem á að kóða:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="107"/>
        <source>Title:</source>
        <translation>Titill:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="139"/>
        <source>URL:</source>
        <translation>Slóð/URL:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="113"/>
        <source>E-Mail address:</source>
        <translation>Tölvupóstfang:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="115"/>
        <source>Subject:</source>
        <translation>Efni:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="117"/>
        <source>Message Body:</source>
        <translation>Meginhluti bréfs:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="150"/>
        <source>Telephone Number:</source>
        <translation>Símanúmer:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="125"/>
        <source>Name:</source>
        <translation>Nafn:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="127"/>
        <source>Telephone:</source>
        <translation>Sími:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="129"/>
        <source>E-Mail:</source>
        <translation>Tölvupóstur:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="145"/>
        <source>Message:</source>
        <translation>Skilaboð:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="147"/>
        <source>characters count: 0</source>
        <translation>Stafafjöldi: 0</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="152"/>
        <source>Content:</source>
        <translation>Innihald:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="156"/>
        <source>Latitude:</source>
        <translation>Breiddargráða:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="158"/>
        <source>Longitude:</source>
        <translation>Lengdargráða:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="179"/>
        <source>Parameters:</source>
        <translation>Viðföng:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="181"/>
        <source>&amp;Pixel Size:</source>
        <translation>Stærð &amp;mynddíla:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="184"/>
        <source>&amp;Error Correction:</source>
        <translation>Villuleiðrétting:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Lowest</source>
        <translation>Lægst</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Medium</source>
        <translation>Miðlungs</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>QuiteGood</source>
        <translation>Góð</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Highest</source>
        <translation>Hæst</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="195"/>
        <source>&amp;Margin Size:</source>
        <translation>Stærð s&amp;pássíu:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="199"/>
        <source>Start typing to create QR Code
 or  drop here image files for decoding.</source>
        <translation>Byrjaðu innslátt til að útbúa QR-kóða eða slepptu myndskrá hingað til að afkóða hana.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="206"/>
        <source>&amp;Save QRCode</source>
        <translation>&amp;Vista QR-kóða</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="207"/>
        <source>&amp;Decode</source>
        <translation>A&amp;fkóða</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="210"/>
        <source>Decode from &amp;File</source>
        <translation>Afkóða frá &amp;skrá</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="211"/>
        <source>Decode from &amp;Webcam</source>
        <translation>Afkóða frá &amp;vefmyndavél</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="214"/>
        <source>E&amp;xit</source>
        <translation>&amp;Hætta</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="216"/>
        <source>&amp;About</source>
        <translation>&amp;Upplýsingar</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="229"/>
        <source>Error Correction Level</source>
        <translation>Stig villuleiðréttingar</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="366"/>
        <source>Select data type:</source>
        <translation>Veldu gerð gagna:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="486"/>
        <source>ERROR: Something went wrong while trying to generate the QR Code.</source>
        <translation>VILLA: Eitthvað fór úrskeiðis við að útbúa QR-kóðann.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="529"/>
        <source>Save QRCode</source>
        <translation>Vista QR-kóða</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="447"/>
        <source>PNG Images (*.png);; All Files (*.*)</source>
        <translation type="obsolete">PNG myndir (*.png);; Allar skrár (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="522"/>
        <source>Save QR Code</source>
        <translation>Vista QR-kóða</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="537"/>
        <source>Open QRCode</source>
        <translation>Opna QR-kóða</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="537"/>
        <source>Images (*.png *.jpg);; All Files (*.*)</source>
        <translation>Myndir (*.png *.jpg);; Allar skrár (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="548"/>
        <source>Decode File</source>
        <translation>Afkóða skrá</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="597"/>
        <source>

Do you want to </source>
        <translation>

Viltu </translation>
    </message>
    <message>
        <location filename="qtqr.py" line="601"/>
        <source>open it in a browser?</source>
        <translation>opna það í vafra?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="602"/>
        <source>send an e-mail to the address?</source>
        <translation>send an e-mail to the address?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="603"/>
        <source>send the e-mail?</source>
        <translation>send the e-mail?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="608"/>
        <source>open it in Google Maps?</source>
        <translation>opna það í Google Maps?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="625"/>
        <source>Decode QRCode</source>
        <translation>Afkóða QR-kóða</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="632"/>
        <source>&amp;Edit</source>
        <translation>Br&amp;eyta</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="717"/>
        <source>Decoding Failed</source>
        <translation>Afkóðun mistókst</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>About QtQR</source>
        <translation>Um QtQR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="522"/>
        <source>QR Code succesfully saved to %s</source>
        <translation>QR-kóði vistaður í %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="529"/>
        <source>QRCode succesfully saved to &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>QR-kóði vistaður í &lt;b&gt;%s&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="548"/>
        <source>No QRCode could be found in file: &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>Enginn QR-kóði fannst í skránni: &lt;b&gt;%s&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="578"/>
        <source>QRCode contains the following text:

%s</source>
        <translation>QR-kóðinn inniheldur eftirfarandi texta:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="579"/>
        <source>QRCode contains the following url address:

%s</source>
        <translation>QR-kóðinn inniheldur eftirfarandi slóð/URL:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="580"/>
        <source>QRCode contains a bookmark:

Title: %s
URL: %s</source>
        <translation>QR-kóðinn inniheldur bókmerki:

Titill: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="581"/>
        <source>QRCode contains the following e-mail address:

%s</source>
        <translation>QR-kóðinn inniheldur eftirfarandi tölvupóstfang:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="582"/>
        <source>QRCode contains an e-mail message:

To: %s
Subject: %s
Message: %s</source>
        <translation>QR-kóðinn inniheldur tölvupóstskilaboð:

Til: %s
Efni: %s
Skilaboð: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="583"/>
        <source>QRCode contains a telephone number: </source>
        <translation>QR-kóðinn inniheldur símanúmer: </translation>
    </message>
    <message>
        <location filename="qtqr.py" line="483"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s</source>
        <translation type="obsolete">QR-kóðinn inniheldur símaskrárfærslu:

Nafn: %s
Sími: %s
Netfang: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="592"/>
        <source>QRCode contains the following SMS message:

To: %s
Message: %s</source>
        <translation>QR-kóðinn inniheldur eftirfarandi SMS skilaboð:

Til: %s
Skilaboð: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="593"/>
        <source>QRCode contains the following MMS message:

To: %s
Message: %s</source>
        <translation>QR-kóðinn inniheldur eftirfarandi MMS skilaboð:

Til: %s
Skilaboð: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="594"/>
        <source>QRCode contains the following coordinates:

Latitude: %s
Longitude:%s</source>
        <translation>QR-kóðinn inniheldur eftirfarandi staðsetningarhnit:

Breiddargráða: %s
Lengdargráða:  %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="402"/>
        <source>characters count: %s - %d message(s)</source>
        <translation>stafafjöldi: %s - %d skilaboð</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="717"/>
        <source>&lt;p&gt;Oops! no code was found.&lt;br /&gt; Maybe your webcam didn&apos;t focus.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Úbbs! enginn kóði fannst.&lt;br /&gt; Hugsanlega fókuseraði vefmyndavélin ekki rétt.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;A simple software for creating and decoding QR Codes that uses &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; as backend. Both are part of the &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; project.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;This is Free Software: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Please visit our website for more information and to check out the code:&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</source>
        <translation>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;Einfaldur hugbúnaður til að búa til og afkóða QR-kóða, stuðst er við  &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; sem bakenda. Hvort tveggja er hluti af &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; verkefninu.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Þetta er frjáls hugbúnaður: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Farðu á vefsvæðið okkar til að skoða ítarlegri upplýsingar og til að sjá kóðann:&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt;&lt;p&gt;Höfundarréttur &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="131"/>
        <source>Note:</source>
        <translation>Athugið:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="133"/>
        <source>Birthday:</source>
        <translation>Afmælisdagur:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="136"/>
        <source>Address:</source>
        <translation>Heimilisfang:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="584"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s
Note: %s
Birthday: %s
Address: %s
URL: %s</source>
        <translation>QR-kóðinn inniheldur færslu í tengiliðaskrá:

Nafn: %s
Sími: %s
Netfang: %s
Aths: %s
Afmælisdagur: %s
Heimilisfang: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="133"/>
        <source>Insert separated by commas the PO Box, room number, house number, city, county, postal code and country in order</source>
        <translation type="obsolete">Settu inn eftirfarandi atriði, aðskilin með kommum; pósthólf, herbergisnúmer, húsnúmer, borg, hérað, póstnúmer og land, í þessari röð</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="55"/>
        <source>WiFi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="138"/>
        <source>Insert separated by commas the PO Box, room number, house number, city, prefecture, zip code and country in order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="162"/>
        <source>SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="164"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="166"/>
        <source>Show Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="167"/>
        <source>Encription:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>WEP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>WPA/WPA2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>No Encription</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="595"/>
        <source>QRCode contains the following WiFi Network Configuration:

SSID: %s
Encription Type: %s
Password: %s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoDevices</name>
    <message>
        <location filename="qtqr.py" line="771"/>
        <source>Decode from Webcam</source>
        <translation>Afkóða frá vefmyndavél</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="777"/>
        <source>You are about to decode from your webcam. Please put the code in front of your camera with a good light source and keep it steady.
Once you see a green rectangle you can close the window by pressing any key.

Please select the video device you want to use for decoding:</source>
        <translation>Þú ert að fara að afkóða frá vefmyndavél. Settu QR-kóðann fyrir framan myndavélina í góðri lýsingu og haltu kóðanum stöðugum.
Þegar þú sérð grænan ferhyrning geturðu lokað glugganum með því að ýta á einhvern lykil.

Veldu myndatökutækið sem þú vilt nota við afkóðunina:</translation>
    </message>
</context>
</TS>
