<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr" sourcelanguage="">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="qtqr.py" line="35"/>
        <source>QtQR: QR Code Generator</source>
        <translation type="unfinished">QtQR: Générateur des Codes QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="46"/>
        <source>Text</source>
        <translation type="unfinished">Texte</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="47"/>
        <source>URL</source>
        <translation type="unfinished">URL</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="48"/>
        <source>Bookmark</source>
        <translation type="unfinished">Signet</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="49"/>
        <source>E-Mail</source>
        <translation type="unfinished">E-Mail</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="50"/>
        <source>Telephone Number</source>
        <translation type="unfinished">Numéro Téléphonique</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="51"/>
        <source>Contact Information (PhoneBook)</source>
        <translation type="unfinished">Information de Contact (Répertoire)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="52"/>
        <source>SMS</source>
        <translation type="unfinished">SMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="53"/>
        <source>MMS</source>
        <translation type="unfinished">MMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="54"/>
        <source>Geolocalization</source>
        <translation type="unfinished">Géolocalisation</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="99"/>
        <source>Text to be encoded:</source>
        <translation type="unfinished">Texte pour encodage:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="103"/>
        <source>URL to be encoded:</source>
        <translation type="unfinished">URL pour encodage:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="107"/>
        <source>Title:</source>
        <translation type="unfinished">Titre:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="139"/>
        <source>URL:</source>
        <translation type="unfinished">URL:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="113"/>
        <source>E-Mail address:</source>
        <translation type="unfinished">Adresse électronique:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="115"/>
        <source>Subject:</source>
        <translation type="unfinished">Sujet:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="117"/>
        <source>Message Body:</source>
        <translation type="unfinished">Corps du Message:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="150"/>
        <source>Telephone Number:</source>
        <translation type="unfinished">Numéro Téléphonique:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="125"/>
        <source>Name:</source>
        <translation type="unfinished">Nom:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="127"/>
        <source>Telephone:</source>
        <translation type="unfinished">Téléphone:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="129"/>
        <source>E-Mail:</source>
        <translation type="unfinished">E-Mail:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="145"/>
        <source>Message:</source>
        <translation type="unfinished">Message:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="147"/>
        <source>characters count: 0</source>
        <translation type="unfinished">compte de caracteres: 0</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="152"/>
        <source>Content:</source>
        <translation type="unfinished">Teneur:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="156"/>
        <source>Latitude:</source>
        <translation type="unfinished">Latitude:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="158"/>
        <source>Longitude:</source>
        <translation type="unfinished">Longitude:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="179"/>
        <source>Parameters:</source>
        <translation type="unfinished">Paramètres:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="181"/>
        <source>&amp;Pixel Size:</source>
        <translation type="unfinished">Taille du &amp;Pixel:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="184"/>
        <source>&amp;Error Correction:</source>
        <translation type="unfinished">Correction d&apos;&amp;Erreur:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Lowest</source>
        <translation type="unfinished">Minimale</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Medium</source>
        <translation type="unfinished">Moyen</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>QuiteGood</source>
        <translation type="unfinished">AssezBon</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Highest</source>
        <translation type="unfinished">Maximale</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="195"/>
        <source>&amp;Margin Size:</source>
        <translation type="unfinished">Taille de la &amp;Marge:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="199"/>
        <source>Start typing to create QR Code
 or  drop here image files for decoding.</source>
        <translation type="unfinished">Commencer taper pour créer un code QR
 ou  laisser ici les fichiers pour décoder.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="206"/>
        <source>&amp;Save QRCode</source>
        <translation type="unfinished">&amp;Sauvegarder le Code QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="207"/>
        <source>&amp;Decode</source>
        <translation type="unfinished">&amp;Décoder</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="210"/>
        <source>Decode from &amp;File</source>
        <translation type="unfinished">Décoder du &amp;Fichier</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="211"/>
        <source>Decode from &amp;Webcam</source>
        <translation type="unfinished">Décoder du &amp;Webcam</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="214"/>
        <source>E&amp;xit</source>
        <translation type="unfinished">&amp;Sortir</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="216"/>
        <source>&amp;About</source>
        <translation type="unfinished">&amp;A propos</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="229"/>
        <source>Error Correction Level</source>
        <translation type="unfinished">Niveau du Correction d&apos;Erreur</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="366"/>
        <source>Select data type:</source>
        <translation type="unfinished">Sélectionnez le type des données:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="486"/>
        <source>ERROR: Something went wrong while trying to generate the QR Code.</source>
        <translation type="unfinished">ERREUR: Quelque chose s&apos;est mal avec le génération du code QR.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="529"/>
        <source>Save QRCode</source>
        <translation type="unfinished">Sauvegarder le Code QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="447"/>
        <source>PNG Images (*.png);; All Files (*.*)</source>
        <translation type="obsolete">Images PNG (*.png);;Toutes les Fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="522"/>
        <source>Save QR Code</source>
        <translation type="unfinished">Sauvegarder le Code QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="537"/>
        <source>Open QRCode</source>
        <translation type="unfinished">Ouvrir un Code QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="537"/>
        <source>Images (*.png *.jpg);; All Files (*.*)</source>
        <translation type="unfinished">Images (*.png *.jpg);;Toutes les Fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="548"/>
        <source>Decode File</source>
        <translation type="unfinished">Décoder un Fichier</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="597"/>
        <source>

Do you want to </source>
        <translation type="unfinished">

Est ce que vous voulez </translation>
    </message>
    <message>
        <location filename="qtqr.py" line="601"/>
        <source>open it in a browser?</source>
        <translation type="unfinished">ouvrir dans un navigateur?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="602"/>
        <source>send an e-mail to the address?</source>
        <translation type="unfinished">Envoyer un e-mail à l&apos;address?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="603"/>
        <source>send the e-mail?</source>
        <translation type="unfinished">Envoyer l&apos;e-mail?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="608"/>
        <source>open it in Google Maps?</source>
        <translation type="unfinished">ouvrir dans Google Maps?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="625"/>
        <source>Decode QRCode</source>
        <translation type="unfinished">Décoder Code QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="632"/>
        <source>&amp;Edit</source>
        <translation type="unfinished">&amp;Editer</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="717"/>
        <source>Decoding Failed</source>
        <translation type="unfinished">Echec de Décodage</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>About QtQR</source>
        <translation type="unfinished">A propros QtQR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="522"/>
        <source>QR Code succesfully saved to %s</source>
        <translation type="unfinished">Code QR etait sauvegardé à %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="529"/>
        <source>QRCode succesfully saved to &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation type="unfinished">Code QR etait sauvegardé à &lt;b&gt;%s&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="548"/>
        <source>No QRCode could be found in file: &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation type="unfinished">Ne peux pas trouver un Code QR dans le fichier &lt;b&gt;%s&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="578"/>
        <source>QRCode contains the following text:

%s</source>
        <translation type="unfinished">Le Code QR contient le texte suivant:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="579"/>
        <source>QRCode contains the following url address:

%s</source>
        <translation type="unfinished">Le Code QR contient le URL suivant:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="580"/>
        <source>QRCode contains a bookmark:

Title: %s
URL: %s</source>
        <translation type="unfinished">Le Code QR contient un signet:

Titre: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="581"/>
        <source>QRCode contains the following e-mail address:

%s</source>
        <translation type="unfinished">Le Code QR contient l&apos;adresse électronique suivant:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="582"/>
        <source>QRCode contains an e-mail message:

To: %s
Subject: %s
Message: %s</source>
        <translation type="unfinished">Le Code QR contient un adresse électronique:

A: %s
Sujet: %s
Message: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="583"/>
        <source>QRCode contains a telephone number: </source>
        <translation type="unfinished">Le Code QR contient un numéro téléphonique:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="483"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s</source>
        <translation type="obsolete">El código QR contiene el siguiente entrada de agenda:

Nombre: %s
Teléfono: %s
E-Mail: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="592"/>
        <source>QRCode contains the following SMS message:

To: %s
Message: %s</source>
        <translation type="unfinished">Le Code QR contient le SMS suivant:

A: %s
Message: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="593"/>
        <source>QRCode contains the following MMS message:

To: %s
Message: %s</source>
        <translation type="unfinished">Le Code QR contient le MMS suivant::

A: %s
Message: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="594"/>
        <source>QRCode contains the following coordinates:

Latitude: %s
Longitude:%s</source>
        <translation type="unfinished">Le Code QR contient les coordonnées suivant::

Latitude: %s
Longitude: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="402"/>
        <source>characters count: %s - %d message(s)</source>
        <translation type="unfinished">compte de caracteres:%s - %d message(s)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="717"/>
        <source>&lt;p&gt;Oops! no code was found.&lt;br /&gt; Maybe your webcam didn&apos;t focus.&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;Houp! pas de code etait trouvait &lt;br /&gt;Peut-être le webcam n&apos;a pas cadré.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;A simple software for creating and decoding QR Codes that uses &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; as backend. Both are part of the &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; project.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;This is Free Software: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Please visit our website for more information and to check out the code:&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</source>
        <translation type="unfinished">&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;Un logiciels simple pour la création et décodage des codes QR que utilise &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; comme un backend. Tous les deux sont partie du &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; .&lt;/p&gt;
&lt;p&gt;&lt;/p&gt;&lt;p&gt;Ce logiciel est gratuit: GNU-GPLv3&lt;/p&gt; &lt;p&gt;&lt;/p&gt;&lt;p&gt;Visitez notre site web pour plus de l&apos;information et pour voir le code:&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt; &lt;p&gt;Droit d&apos;auteur &amp;copy; Ramiro Algozino 
&amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="131"/>
        <source>Note:</source>
        <translation type="unfinished">Note:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="133"/>
        <source>Birthday:</source>
        <translation type="unfinished">Anniversaire:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="136"/>
        <source>Address:</source>
        <translation type="unfinished">Adresse:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="584"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s
Note: %s
Birthday: %s
Address: %s
URL: %s</source>
        <translation type="unfinished">Le Code QR contient un entrée du répertoire:

Numéro: %s
Téléphone: %s
E-Mail: %s
Notes: %s
Anniversaire: %s
Adresse: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="138"/>
        <source>Insert separated by commas the PO Box, room number, house number, city, prefecture, zip code and country in order</source>
        <translation type="unfinished">Insérez, séparé par virgules,le boîte postale, numéro de chambre, numéro de la maison, la ville, préfecture, code postal, et pays arrangé</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="55"/>
        <source>WiFi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="162"/>
        <source>SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="164"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="166"/>
        <source>Show Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="167"/>
        <source>Encription:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>WEP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>WPA/WPA2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>No Encription</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="595"/>
        <source>QRCode contains the following WiFi Network Configuration:

SSID: %s
Encription Type: %s
Password: %s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoDevices</name>
    <message>
        <location filename="qtqr.py" line="771"/>
        <source>Decode from Webcam</source>
        <translation type="unfinished">Décoder avec le Webcam</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="777"/>
        <source>You are about to decode from your webcam. Please put the code in front of your camera with a good light source and keep it steady.
Once you see a green rectangle you can close the window by pressing any key.

Please select the video device you want to use for decoding:</source>
        <translation type="unfinished">Vous allez décoder avec votre webcam. Tenez le code en face de votre caméra avec une bon source de lumière et assurer sa stabilité s&apos;il vous plaît. Quand vous voyez un rectangle vert, vous pouvez fermer la fenêtre en appuyant sur quelconque clé.

Sélectionnez le périphérique vidéo que vous voulez utiliser pour décoder s&apos;il vous plaît:</translation>
    </message>
</context>
</TS>
