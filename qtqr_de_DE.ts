<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="de_DE" sourcelanguage="">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="qtqr.py" line="35"/>
        <source>QtQR: QR Code Generator</source>
        <translation>QtQR: QR Code Generator</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="46"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="47"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="48"/>
        <source>Bookmark</source>
        <translation>Lesezeichen</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="49"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="50"/>
        <source>Telephone Number</source>
        <translation>Telefonnummer</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="51"/>
        <source>Contact Information (PhoneBook)</source>
        <translation>Kontakt (Telefonbuch)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="52"/>
        <source>SMS</source>
        <translation>SMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="53"/>
        <source>MMS</source>
        <translation>MMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="54"/>
        <source>Geolocalization</source>
        <translation>Geographischer Standort</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="99"/>
        <source>Text to be encoded:</source>
        <translation>Zu kodierender Text:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="103"/>
        <source>URL to be encoded:</source>
        <translation>Zu kodierende URL:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="107"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="139"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="113"/>
        <source>E-Mail address:</source>
        <translation>E-Mail Adresse:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="115"/>
        <source>Subject:</source>
        <translation>Betreff:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="117"/>
        <source>Message Body:</source>
        <translation>Nachrichtentext:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="150"/>
        <source>Telephone Number:</source>
        <translation>Telefonnummer:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="125"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="127"/>
        <source>Telephone:</source>
        <translation>Telefon:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="129"/>
        <source>E-Mail:</source>
        <translation>E-Mail:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="145"/>
        <source>Message:</source>
        <translation>Nachricht:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="147"/>
        <source>characters count: 0</source>
        <translation>Zeichen Zähler: 0</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="152"/>
        <source>Content:</source>
        <translation>Inhalt:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="156"/>
        <source>Latitude:</source>
        <translation>Breitengrad:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="158"/>
        <source>Longitude:</source>
        <translation>Längengrad:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="179"/>
        <source>Parameters:</source>
        <translation>Parameter:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="181"/>
        <source>&amp;Pixel Size:</source>
        <translation>&amp;Punktgröße:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="184"/>
        <source>&amp;Error Correction:</source>
        <translation>&amp;Fehlerkorrektur:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Lowest</source>
        <translation>Niedrigste</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Medium</source>
        <translation>Mittel</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>QuiteGood</source>
        <translation>Recht gut</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Highest</source>
        <translation>Höchste</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="195"/>
        <source>&amp;Margin Size:</source>
        <translation>&amp;Randgröße:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="199"/>
        <source>Start typing to create QR Code
 or  drop here image files for decoding.</source>
        <translation>Beginne zu tippen um QR Code zu erstellen oder
 ziehe Bilddateien zum Dekodieren in dieses Fenster.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="206"/>
        <source>&amp;Save QRCode</source>
        <translation>&amp;QRCode Speichern</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="207"/>
        <source>&amp;Decode</source>
        <translation>&amp;Dekodieren</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="210"/>
        <source>Decode from &amp;File</source>
        <translation>Dekodiere von einer &amp;Datei</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="211"/>
        <source>Decode from &amp;Webcam</source>
        <translation>Dekodiere von &amp;Webcam</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="214"/>
        <source>E&amp;xit</source>
        <translation>&amp;Verlassen</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="216"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="229"/>
        <source>Error Correction Level</source>
        <translation>Grad der Fehlerkorrektur</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="366"/>
        <source>Select data type:</source>
        <translation>Datentyp wählen:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="486"/>
        <source>ERROR: Something went wrong while trying to generate the QR Code.</source>
        <translation>FEHLER: Beim Versuch den QR Code zu generieren ist etwas schiefgegangen.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="529"/>
        <source>Save QRCode</source>
        <translation>QRCode Speichern</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="447"/>
        <source>PNG Images (*.png);; All Files (*.*)</source>
        <translation type="obsolete">PNG Bilder (*.png);; Alle Dateien (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="522"/>
        <source>Save QR Code</source>
        <translation>QR Code Speichern</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="537"/>
        <source>Open QRCode</source>
        <translation>QRCode Öffnen</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="537"/>
        <source>Images (*.png *.jpg);; All Files (*.*)</source>
        <translation>Bilder (*.png *.jpg);; Alle Dateien (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="548"/>
        <source>Decode File</source>
        <translation>Dekodiere Datei</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="597"/>
        <source>

Do you want to </source>
        <translation>

Wollen Sie </translation>
    </message>
    <message>
        <location filename="qtqr.py" line="601"/>
        <source>open it in a browser?</source>
        <translation>sie im Browser öffnen?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="602"/>
        <source>send an e-mail to the address?</source>
        <translation>eine E-Mail zu der Adresse senden?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="603"/>
        <source>send the e-mail?</source>
        <translation>die E-Mail senden?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="608"/>
        <source>open it in Google Maps?</source>
        <translation>in Google Maps öffnen?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="625"/>
        <source>Decode QRCode</source>
        <translation>Dekodiere QRCode</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="632"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="717"/>
        <source>Decoding Failed</source>
        <translation>Dekodieren fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>About QtQR</source>
        <translation>Über QtQR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="522"/>
        <source>QR Code succesfully saved to %s</source>
        <translation>QR Code erfolgreich gespeichert unter %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="529"/>
        <source>QRCode succesfully saved to &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>QR Code erfolgreich gespeichert unter &lt;b&gt;%s&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="548"/>
        <source>No QRCode could be found in file: &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>Es konnte kein QRCode gefunden werden in Datei: &lt;b&gt;%s&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="578"/>
        <source>QRCode contains the following text:

%s</source>
        <translation>QRCode enthält folgenden Text:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="579"/>
        <source>QRCode contains the following url address:

%s</source>
        <translation>QRCode enthält folgende url Adresse:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="580"/>
        <source>QRCode contains a bookmark:

Title: %s
URL: %s</source>
        <translation>QRCode enthält ein Lesezeichen:

Titel: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="581"/>
        <source>QRCode contains the following e-mail address:

%s</source>
        <translation>QRCode enthält folgenden E-Mail Adresse:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="582"/>
        <source>QRCode contains an e-mail message:

To: %s
Subject: %s
Message: %s</source>
        <translation>QRCode enthält eine E-Mail Nachricht:

An: %s
Betreff: %s
Nachricht: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="583"/>
        <source>QRCode contains a telephone number: </source>
        <translation>QRCode enthält eine Telefonnummer: </translation>
    </message>
    <message>
        <location filename="qtqr.py" line="483"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s</source>
        <translation type="obsolete">QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="592"/>
        <source>QRCode contains the following SMS message:

To: %s
Message: %s</source>
        <translation>QRCode enthält folgende SMS Nachricht:

An: %s
Nachricht: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="593"/>
        <source>QRCode contains the following MMS message:

To: %s
Message: %s</source>
        <translation>QRCode enthält folgende MMS Nachricht:

Ab: %s
Nachricht: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="594"/>
        <source>QRCode contains the following coordinates:

Latitude: %s
Longitude:%s</source>
        <translation>QRCode enthält folgende Koordinaten:

Breitengrad: %s
Längengrad:%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="402"/>
        <source>characters count: %s - %d message(s)</source>
        <translation>Zeichen Zähler: %s - %d Nachricht(en)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="717"/>
        <source>&lt;p&gt;Oops! no code was found.&lt;br /&gt; Maybe your webcam didn&apos;t focus.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Oops! Kein code gefunden.&lt;br /&gt; Vielleicht hat Ihre Webcam nicht fokusiert.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;A simple software for creating and decoding QR Codes that uses &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; as backend. Both are part of the &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; project.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;This is Free Software: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Please visit our website for more information and to check out the code:&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</source>
        <translation>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;A simple software for creating and decoding QR Codes that uses &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; as backend. Both are part of the &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; project.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;This is Free Software: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Please visit our website for more information and to check out the code:&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="131"/>
        <source>Note:</source>
        <translation>Bemerkung:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="133"/>
        <source>Birthday:</source>
        <translation>Geburtsdatum:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="136"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="584"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s
Note: %s
Birthday: %s
Address: %s
URL: %s</source>
        <translation>QRCode enthält einen Telefonbuch-Eintrag:

Name: %s
Tel: %s
E-Mail: %s
Bemerkung: %s
Geburtsdatum: %s
Adresse: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="138"/>
        <source>Insert separated by commas the PO Box, room number, house number, city, prefecture, zip code and country in order</source>
        <translation>Füge durch Kommas getrennt die PO Box, Raum-Nummer, Hausnummer, Ort, Land, Postleitzahl und country in order</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="55"/>
        <source>WiFi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="162"/>
        <source>SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="164"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="166"/>
        <source>Show Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="167"/>
        <source>Encription:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>WEP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>WPA/WPA2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>No Encription</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="595"/>
        <source>QRCode contains the following WiFi Network Configuration:

SSID: %s
Encription Type: %s
Password: %s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoDevices</name>
    <message>
        <location filename="qtqr.py" line="771"/>
        <source>Decode from Webcam</source>
        <translation>Dekodiere von Webcam</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="777"/>
        <source>You are about to decode from your webcam. Please put the code in front of your camera with a good light source and keep it steady.
Once you see a green rectangle you can close the window by pressing any key.

Please select the video device you want to use for decoding:</source>
        <translation>Sie sind dabei von Ihrer Webcam zu dekodieren. Bitte bringen Sie den Code vor Ihre Kamera mit guten Lichtverhältnissen und halten Sie ihn ruhig.
Sobald Sie ein grünes Rechteck sehen können Sie das Fester mit Drücken einer beliebigen Taste schließen.

Bitte wählen Sie das Videogerät welches sie zum dekodieren benutzen wollen:</translation>
    </message>
</context>
</TS>
