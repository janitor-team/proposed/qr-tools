<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="es_AR" sourcelanguage="">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="qtqr.py" line="35"/>
        <source>QtQR: QR Code Generator</source>
        <translation>QtQR: Generador de Códigos QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="46"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="47"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="48"/>
        <source>Bookmark</source>
        <translation>Marcador</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="49"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="50"/>
        <source>Telephone Number</source>
        <translation>Número Telefónico</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="51"/>
        <source>Contact Information (PhoneBook)</source>
        <translation>Información de Contacto (Agenda)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="52"/>
        <source>SMS</source>
        <translation>SMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="53"/>
        <source>MMS</source>
        <translation>MMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="54"/>
        <source>Geolocalization</source>
        <translation>Geolocalización</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="103"/>
        <source>Text to be encoded:</source>
        <translation>Texto a ser codificado:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="107"/>
        <source>URL to be encoded:</source>
        <translation>URL a ser codificada:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="111"/>
        <source>Title:</source>
        <translation>Título:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="143"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="117"/>
        <source>E-Mail address:</source>
        <translation>Dirección de E-Mail:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="119"/>
        <source>Subject:</source>
        <translation>Asunto:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="121"/>
        <source>Message Body:</source>
        <translation>Cuerpo del Mensaje:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="154"/>
        <source>Telephone Number:</source>
        <translation>Número de Teléfono:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="183"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="131"/>
        <source>Telephone:</source>
        <translation>Teléfono:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="133"/>
        <source>E-Mail:</source>
        <translation>E-Mail:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="149"/>
        <source>Message:</source>
        <translation>Mensaje:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="151"/>
        <source>characters count: 0</source>
        <translation>Cantidad de caracteres: 0</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="156"/>
        <source>Content:</source>
        <translation>Contenido:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="160"/>
        <source>Latitude:</source>
        <translation>Latitud:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="162"/>
        <source>Longitude:</source>
        <translation>Longitud:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="202"/>
        <source>Parameters:</source>
        <translation>Parámetros:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="204"/>
        <source>&amp;Pixel Size:</source>
        <translation>Tamaño de &amp;Píxel:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="207"/>
        <source>&amp;Error Correction:</source>
        <translation>Corrección de &amp;Error:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="209"/>
        <source>Lowest</source>
        <translation>Mínima</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="209"/>
        <source>Medium</source>
        <translation>Medio</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="209"/>
        <source>QuiteGood</source>
        <translation>Buena</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="209"/>
        <source>Highest</source>
        <translation>Máxima</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="218"/>
        <source>&amp;Margin Size:</source>
        <translation>Tamaño del &amp;Márgen:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="222"/>
        <source>Start typing to create QR Code
 or  drop here image files for decoding.</source>
        <translation>Comenzá a escribir para generar el Código QR
o soltá un archivo imagen con el código acá.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="229"/>
        <source>&amp;Save QRCode</source>
        <translation>&amp;Guardar Código QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="230"/>
        <source>&amp;Decode</source>
        <translation>&amp;Decodificar</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="232"/>
        <source>Decode from &amp;File</source>
        <translation>Decodificar desde &amp;Archivo</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="233"/>
        <source>Decode from &amp;Webcam</source>
        <translation>Decodificar desde la &amp;Webcam</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="235"/>
        <source>E&amp;xit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="237"/>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="250"/>
        <source>Error Correction Level</source>
        <translation>Nivel de Corrección de Erorres</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="405"/>
        <source>Select data type:</source>
        <translation>Seleccioná el tipo de datos:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="564"/>
        <source>ERROR: Something went wrong while trying to generate the QR Code.</source>
        <translation>ERROR: Algo salió mal tratando de generar el código QR.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="609"/>
        <source>Save QRCode</source>
        <translation>Guardar Código QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="447"/>
        <source>PNG Images (*.png);; All Files (*.*)</source>
        <translation type="obsolete">Imágenes PNG (*.png);;Todos los archivos (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="602"/>
        <source>Save QR Code</source>
        <translation>Guardar Código QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="617"/>
        <source>Open QRCode</source>
        <translation>Abrir Código QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="617"/>
        <source>Images (*.png *.jpg);; All Files (*.*)</source>
        <translation>Imágenes (*.png *.jpg);;Todos los Archivos (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="636"/>
        <source>Decode File</source>
        <translation>Decodificar archivo</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="693"/>
        <source>

Do you want to </source>
        <translation>

¿Querés </translation>
    </message>
    <message>
        <location filename="qtqr.py" line="697"/>
        <source>open it in a browser?</source>
        <translation>abrirlo en tu navegador?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="698"/>
        <source>send an e-mail to the address?</source>
        <translation>manda un e-mail a la dirección?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="699"/>
        <source>send the e-mail?</source>
        <translation>mandar el e-mail?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="704"/>
        <source>open it in Google Maps?</source>
        <translation>abrirlo en Google Maps?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="721"/>
        <source>Decode QRCode</source>
        <translation>Decodificar Código QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="824"/>
        <source>Decoding Failed</source>
        <translation>Falló la Decodificación</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="835"/>
        <source>About QtQR</source>
        <translation>Acerca de QtQR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="602"/>
        <source>QR Code succesfully saved to %s</source>
        <translation>El código QR se guardó satisfactoriamente en %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="609"/>
        <source>QRCode succesfully saved to &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>El código QR se guardó satisfactoriamente en &lt;b&gt;%s&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="636"/>
        <source>No QRCode could be found in file: &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>No se pudo encontrar un código QR en el archivo &lt;b&gt;%s&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="666"/>
        <source>QRCode contains the following text:

%s</source>
        <translation>El código QR contiene el siguiente texto:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="667"/>
        <source>QRCode contains the following url address:

%s</source>
        <translation>El código QR contiene la siguiente URL:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="668"/>
        <source>QRCode contains a bookmark:

Title: %s
URL: %s</source>
        <translation>El código QR contiene el marcador:

Título: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="669"/>
        <source>QRCode contains the following e-mail address:

%s</source>
        <translation>El código QR contiene la siguiente dirección de e-mail:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="670"/>
        <source>QRCode contains an e-mail message:

To: %s
Subject: %s
Message: %s</source>
        <translation>El código QR contiene el siguiente e-mail:

Para: %s
Asunto: %s
Mensaje: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="671"/>
        <source>QRCode contains a telephone number: </source>
        <translation>El código QR contiene el siguiente número telefónico: </translation>
    </message>
    <message>
        <location filename="qtqr.py" line="483"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s</source>
        <translation type="obsolete">El código QR contiene el siguiente entrada de agenda:

Nombre: %s
Teléfono: %s
E-Mail: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="680"/>
        <source>QRCode contains the following SMS message:

To: %s
Message: %s</source>
        <translation>El código QR contiene el siguiente SMS:

Para: %s
Mensaje: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="681"/>
        <source>QRCode contains the following MMS message:

To: %s
Message: %s</source>
        <translation>El código QR contiene el siguiente MMS:

Para: %s
Mensaje: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="682"/>
        <source>QRCode contains the following coordinates:

Latitude: %s
Longitude:%s</source>
        <translation>El código QR contiene las siguientes coordenadas:

Latitud: %s
Longitud: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="441"/>
        <source>characters count: %s - %d message(s)</source>
        <translation>cantidad de caracteres: %s - %d mensaje(s)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="824"/>
        <source>&lt;p&gt;Oops! no code was found.&lt;br /&gt; Maybe your webcam didn&apos;t focus.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Caray! no encontramos ningún código.&lt;br /&gt;Talvez la webcam no hizo foco.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;A simple software for creating and decoding QR Codes that uses &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; as backend. Both are part of the &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; project.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;This is Free Software: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Please visit our website for more information and to check out the code:&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h1&gt;QtQR %s&lt;/h1&gt;
&lt;p&gt;Un simple software para crear y decodificar códigos QR que utiliza &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; como backend. Ambos son parte del proyecto &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; .&lt;/p&gt;
&lt;p&gt;&lt;/p&gt;
&lt;p&gt;Estos es software gratuito: GNU-GPLv3&lt;/p&gt; 
&lt;p&gt;&lt;/p&gt;
&lt;p&gt;Por favor, visitá nuestro sitio web para más información y para obtener el códio fuente:&lt;br /&gt;
&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;
https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt; 
&lt;p&gt;copyright &amp;copy; Ramiro Algozino 
&amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="135"/>
        <source>Note:</source>
        <translation>Notas:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="137"/>
        <source>Birthday:</source>
        <translation>Fecha de Nacimiento:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="140"/>
        <source>Address:</source>
        <translation>Dirección:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="672"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s
Note: %s
Birthday: %s
Address: %s
URL: %s</source>
        <translation>El código QR contiene el siguiente entrada de agenda:

Nombre: %s
Teléfono: %s
E-Mail: %s
Notas: %s
Fecha de Nacimiento: %s
Dirección: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="142"/>
        <source>Insert separated by commas the PO Box, room number, house number, city, prefecture, zip code and country in order</source>
        <translation>Insertá, separado por comas, el Buzón, número de habitación, número de puerta, ciudad, estado, código postal y país en orden</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="55"/>
        <source>WiFi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="166"/>
        <source>SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="168"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="170"/>
        <source>Show Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="173"/>
        <source>WEP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="173"/>
        <source>WPA/WPA2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="56"/>
        <source>SEPA Single Payment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="171"/>
        <source>Encryption:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="173"/>
        <source>No Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="185"/>
        <source>Account:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="187"/>
        <source>BNC:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="189"/>
        <source>Amount:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="191"/>
        <source>Reason:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="193"/>
        <source>Currency:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="683"/>
        <source>QRCode contains the following WiFi Network Configuration:

SSID: %s
Encryption Type: %s
Password: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="684"/>
        <source>QRCode contains the following Single Payment Information:

Name: %s
Account: %s
BNC: %s
Ammount: %s
Reason: %s
Currency: %s
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="835"/>
        <source>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;A simple software for creating and decoding QR Codes that uses python-qrtools as backend. Both are part of the &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; project.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;This is Free Software: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Please visit our website for more information and to check out the code:&lt;br /&gt;&lt;a href=&quot;https://code.launchpad.net/qr-tools/&quot;&gt;https://code.launchpad.net/qr-tools/&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoDevices</name>
    <message>
        <location filename="qtqr.py" line="879"/>
        <source>Decode from Webcam</source>
        <translation>Decodificar desde la Webcam</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="777"/>
        <source>You are about to decode from your webcam. Please put the code in front of your camera with a good light source and keep it steady.
Once you see a green rectangle you can close the window by pressing any key.

Please select the video device you want to use for decoding:</source>
        <translation type="obsolete">Estás a punto de decodificar desde la webcam. Por favor, poné el código QR en frente de tu cámara con una buena fuente de luz y mantenelo quieto.
Una vez que veas un recuadro verde alrededor del código podés cerrar la ventana presionando cualquier tecla.

Por favor seleccioná el dispositivo de video que querés utilizar:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="884"/>
        <source>You are about to decode from your webcam. Please put the code in front of your camera with a good light source and keep it steady.
QtQR will try to detect automatically the QR Code.

Please select the video device you want to use for decoding:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
