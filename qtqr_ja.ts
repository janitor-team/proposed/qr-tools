<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="ja" sourcelanguage="">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="qtqr.py" line="35"/>
        <source>QtQR: QR Code Generator</source>
        <translation>QtQR: QRコード生成器</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="46"/>
        <source>Text</source>
        <translation>テキスト</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="47"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="48"/>
        <source>Bookmark</source>
        <translation>ブックマーク</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="49"/>
        <source>E-Mail</source>
        <translation>Eメール</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="50"/>
        <source>Telephone Number</source>
        <translation>電話番号</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="51"/>
        <source>Contact Information (PhoneBook)</source>
        <translation>連絡先情報 (電話帳)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="52"/>
        <source>SMS</source>
        <translation>SMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="53"/>
        <source>MMS</source>
        <translation>MMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="54"/>
        <source>Geolocalization</source>
        <translation>位置情報</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="99"/>
        <source>Text to be encoded:</source>
        <translation>エンコードするテキスト:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="103"/>
        <source>URL to be encoded:</source>
        <translation>エンコードするURL:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="107"/>
        <source>Title:</source>
        <translation>タイトル:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="139"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="113"/>
        <source>E-Mail address:</source>
        <translation>Eメールアドレス:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="115"/>
        <source>Subject:</source>
        <translation>件名:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="117"/>
        <source>Message Body:</source>
        <translation>メッセージ本文:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="150"/>
        <source>Telephone Number:</source>
        <translation>電話番号:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="125"/>
        <source>Name:</source>
        <translation>名前:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="127"/>
        <source>Telephone:</source>
        <translation>電話:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="129"/>
        <source>E-Mail:</source>
        <translation>Eメール:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="145"/>
        <source>Message:</source>
        <translation>本文:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="147"/>
        <source>characters count: 0</source>
        <translation>文字数: 0</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="152"/>
        <source>Content:</source>
        <translation>内容:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="156"/>
        <source>Latitude:</source>
        <translation>緯度:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="158"/>
        <source>Longitude:</source>
        <translation>経度:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="179"/>
        <source>Parameters:</source>
        <translation>パラメーター:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="181"/>
        <source>&amp;Pixel Size:</source>
        <translation>ピクセルサイズ(&amp;P):</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="184"/>
        <source>&amp;Error Correction:</source>
        <translation>エラー訂正(&amp;E):</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Lowest</source>
        <translation>最低</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Medium</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>QuiteGood</source>
        <translation>まあまあ</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Highest</source>
        <translation>最高</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="195"/>
        <source>&amp;Margin Size:</source>
        <translation>余白(&amp;M):</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="199"/>
        <source>Start typing to create QR Code
 or  drop here image files for decoding.</source>
        <translation>文字列を入力してQRコードを生成するか、
ここに画像ファイルをドロップしてデコードします。</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="206"/>
        <source>&amp;Save QRCode</source>
        <translation>QRコードを保存(&amp;S)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="207"/>
        <source>&amp;Decode</source>
        <translation>デコード(&amp;D)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="210"/>
        <source>Decode from &amp;File</source>
        <translation>ファイルからデコード(&amp;F)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="211"/>
        <source>Decode from &amp;Webcam</source>
        <translation>Webカメラからデコード(&amp;W)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="214"/>
        <source>E&amp;xit</source>
        <translation>終了(&amp;X)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="216"/>
        <source>&amp;About</source>
        <translation>情報(&amp;A)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="229"/>
        <source>Error Correction Level</source>
        <translation>エラー訂正レベル</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="366"/>
        <source>Select data type:</source>
        <translation>データの形式を選択してください:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="486"/>
        <source>ERROR: Something went wrong while trying to generate the QR Code.</source>
        <translation>エラー: QRコードの生成中に何らかの問題が発生しました。</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="529"/>
        <source>Save QRCode</source>
        <translation>QRコードを保存</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="447"/>
        <source>PNG Images (*.png);; All Files (*.*)</source>
        <translation type="obsolete">PNG画像 (*.png);;すべてのファイル (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="522"/>
        <source>Save QR Code</source>
        <translation>QRコードを保存</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="537"/>
        <source>Open QRCode</source>
        <translation>QRコードを開く</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="537"/>
        <source>Images (*.png *.jpg);; All Files (*.*)</source>
        <translation>画像 (*.png *.jpg);; すべてのファイル (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="548"/>
        <source>Decode File</source>
        <translation>ファイルをデコード</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="597"/>
        <source>

Do you want to </source>
        <translation>

</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="601"/>
        <source>open it in a browser?</source>
        <translation>Webブラウザーで開きますか？</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="602"/>
        <source>send an e-mail to the address?</source>
        <translation>宛先にEメールを送りますか？</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="603"/>
        <source>send the e-mail?</source>
        <translation>Eメールを送信しますか？</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="608"/>
        <source>open it in Google Maps?</source>
        <translation>Google マップで開きますか？</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="625"/>
        <source>Decode QRCode</source>
        <translation>QRコードをデコード</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="632"/>
        <source>&amp;Edit</source>
        <translation>編集(&amp;E)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="717"/>
        <source>Decoding Failed</source>
        <translation>デコードに失敗しました</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>About QtQR</source>
        <translation>QtQR について</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="522"/>
        <source>QR Code succesfully saved to %s</source>
        <translation>QRコードは %s に正しく保存されました</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="529"/>
        <source>QRCode succesfully saved to &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>QRコードは &lt;b&gt;%s&lt;/b&gt; に正しく保存されました。</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="548"/>
        <source>No QRCode could be found in file: &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>&lt;b&gt;%s&lt;/b&gt; の中にQRコードは見つかりませんでした。</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="578"/>
        <source>QRCode contains the following text:

%s</source>
        <translation>QRコードには次のテキストが含まれています:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="579"/>
        <source>QRCode contains the following url address:

%s</source>
        <translation>QRコードには次のURLアドレスが含まれています:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="580"/>
        <source>QRCode contains a bookmark:

Title: %s
URL: %s</source>
        <translation>QRコードにはブックマークが含まれています:

タイトル: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="581"/>
        <source>QRCode contains the following e-mail address:

%s</source>
        <translation>QRコードには次のEメールアドレスが含まれています:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="582"/>
        <source>QRCode contains an e-mail message:

To: %s
Subject: %s
Message: %s</source>
        <translation>QRコードにはEメールメッセージが含まれています:

宛先: %s
件名: %s
本文: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="583"/>
        <source>QRCode contains a telephone number: </source>
        <translation>QRコードには電話番号が含まれています:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="592"/>
        <source>QRCode contains the following SMS message:

To: %s
Message: %s</source>
        <translation>QRコードには次のSMSメッセージが含まれています:

宛先: %s
本文: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="593"/>
        <source>QRCode contains the following MMS message:

To: %s
Message: %s</source>
        <translation>QRコードには次のMMSメッセージが含まれています:

宛先: %s
本文: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="594"/>
        <source>QRCode contains the following coordinates:

Latitude: %s
Longitude:%s</source>
        <translation>QRコードには次の座標が含まれています:

緯度: %s
経度: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="402"/>
        <source>characters count: %s - %d message(s)</source>
        <translation>文字数: %s - %d メッセージ</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="717"/>
        <source>&lt;p&gt;Oops! no code was found.&lt;br /&gt; Maybe your webcam didn&apos;t focus.&lt;/p&gt;</source>
        <translation>&lt;p&gt;おっと！コードが見つかりませんでした。&lt;br /&gt;おそらくWebカメラのフォーカスが合っていません。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;A simple software for creating and decoding QR Codes that uses &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; as backend. Both are part of the &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; project.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;This is Free Software: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Please visit our website for more information and to check out the code:&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</source>
        <translation>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;QRコードを生成・読み込みするためのシンプルなソフトウェアで、バックエンドに &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; を使用しています。これら2つは &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; プロジェクトの一部です。&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;これはフリーソフトウェアです: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;詳細な情報を得たりソースコードを取得するには、Webサイトにアクセスしてください:&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="131"/>
        <source>Note:</source>
        <translation>メモ:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="133"/>
        <source>Birthday:</source>
        <translation>誕生日:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="136"/>
        <source>Address:</source>
        <translation>住所:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="584"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s
Note: %s
Birthday: %s
Address: %s
URL: %s</source>
        <translation>QRコードには電話帳の情報が含まれています:

名前: %s
電話: %s
Eメール: %s
メモ: %s
誕生日: %s
住所: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="138"/>
        <source>Insert separated by commas the PO Box, room number, house number, city, prefecture, zip code and country in order</source>
        <translation>カンマ区切りで順に、私書箱、部屋番号、番地、町名、都道府県、郵便番号、国名を入力してください</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="55"/>
        <source>WiFi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="162"/>
        <source>SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="164"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="166"/>
        <source>Show Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="167"/>
        <source>Encription:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>WEP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>WPA/WPA2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>No Encription</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="595"/>
        <source>QRCode contains the following WiFi Network Configuration:

SSID: %s
Encription Type: %s
Password: %s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoDevices</name>
    <message>
        <location filename="qtqr.py" line="771"/>
        <source>Decode from Webcam</source>
        <translation>Webカメラからデコード</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="777"/>
        <source>You are about to decode from your webcam. Please put the code in front of your camera with a good light source and keep it steady.
Once you see a green rectangle you can close the window by pressing any key.

Please select the video device you want to use for decoding:</source>
        <translation>Webカメラからデコードします。明るい場所でQRコードをカメラの前に置き、あまり動かないようにしてください。
緑色の四角形が表示されたら、何らかのキーを押して画面を閉じることができます。

デコードに使いたいビデオデバイスを選択してください:</translation>
    </message>
</context>
</TS>
