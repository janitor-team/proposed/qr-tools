<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="qtqr.py" line="35"/>
        <source>QtQR: QR Code Generator</source>
        <translation>QtQR: QR Code Generator</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="46"/>
        <source>Text</source>
        <translation>Testo</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="47"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="48"/>
        <source>Bookmark</source>
        <translation>Segnalibro</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="49"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="50"/>
        <source>Telephone Number</source>
        <translation>Numero di telefono</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="51"/>
        <source>Contact Information (PhoneBook)</source>
        <translation>Contatto</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="52"/>
        <source>SMS</source>
        <translation>SMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="53"/>
        <source>MMS</source>
        <translation>MMS</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="54"/>
        <source>Geolocalization</source>
        <translation>Geolocalizzazione</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="55"/>
        <source>WiFi Network</source>
        <translation>Rete WiFi</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="56"/>
        <source>SEPA Single Payment</source>
        <translation>Bonifico SEPA</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="103"/>
        <source>Text to be encoded:</source>
        <translation>Testo da codificare:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="107"/>
        <source>URL to be encoded:</source>
        <translation>URL da codificare:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="111"/>
        <source>Title:</source>
        <translation>Titolo:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="143"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="117"/>
        <source>E-Mail address:</source>
        <translation>Indirizzo E-Mail:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="119"/>
        <source>Subject:</source>
        <translation>Oggetto:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="121"/>
        <source>Message Body:</source>
        <translation>Corpo del messaggio:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="154"/>
        <source>Telephone Number:</source>
        <translation>Numero di telefono:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="183"/>
        <source>Name:</source>
        <translation>Nome:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="131"/>
        <source>Telephone:</source>
        <translation>Telefono:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="133"/>
        <source>E-Mail:</source>
        <translation>E-Mail:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="135"/>
        <source>Note:</source>
        <translation>Note:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="137"/>
        <source>Birthday:</source>
        <translation>Compleanno:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="140"/>
        <source>Address:</source>
        <translation>Indirizzo:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="142"/>
        <source>Insert separated by commas the PO Box, room number, house number, city, prefecture, zip code and country in order</source>
        <translation>Inserire, in questo ordine e separati da virgola: numero di casella postale, numero di stanza, numero civico, città, provincia, CAP e Paese</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="149"/>
        <source>Message:</source>
        <translation>Messaggio:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="151"/>
        <source>characters count: 0</source>
        <translation>conteggio caratteri: 0</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="156"/>
        <source>Content:</source>
        <translation>Contenuto:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="160"/>
        <source>Latitude:</source>
        <translation>Latitudine:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="162"/>
        <source>Longitude:</source>
        <translation>Longitudine:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="166"/>
        <source>SSID:</source>
        <translation>SSID:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="168"/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="170"/>
        <source>Show Password</source>
        <translation>Mostra password</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="171"/>
        <source>Encryption:</source>
        <translation>Cifratura:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="173"/>
        <source>WEP</source>
        <translation>WEP</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="173"/>
        <source>WPA/WPA2</source>
        <translation>WPA/WPA2</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="173"/>
        <source>No Encryption</source>
        <translation>Aperta</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="185"/>
        <source>Account:</source>
        <translation>CC:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="187"/>
        <source>BNC:</source>
        <translation>BNC:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="189"/>
        <source>Amount:</source>
        <translation>Importo:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="191"/>
        <source>Reason:</source>
        <translation>Causale:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="193"/>
        <source>Currency:</source>
        <translation>Valuta:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="202"/>
        <source>Parameters:</source>
        <translation>Parametri:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="204"/>
        <source>&amp;Pixel Size:</source>
        <translation>Dimensione &amp;Pixel:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="207"/>
        <source>&amp;Error Correction:</source>
        <translation>Correzione &amp;Errori:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="209"/>
        <source>Lowest</source>
        <translation>Bassa</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="209"/>
        <source>Medium</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="209"/>
        <source>QuiteGood</source>
        <translation>Buona</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="209"/>
        <source>Highest</source>
        <translation>Alta</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="218"/>
        <source>&amp;Margin Size:</source>
        <translation>Dimensione &amp;Margine:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="222"/>
        <source>Start typing to create QR Code
 or  drop here image files for decoding.</source>
        <translation>Inizia a scrivere per creare un codice QR
o trascina qui i file immagine da decodificare.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="229"/>
        <source>&amp;Save QRCode</source>
        <translation>&amp;Salva codice QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="230"/>
        <source>&amp;Decode</source>
        <translation>&amp;Decodifica</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="232"/>
        <source>Decode from &amp;File</source>
        <translation>Decodifica da &amp;File</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="233"/>
        <source>Decode from &amp;Webcam</source>
        <translation>Decodifica da &amp;Webcam</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="235"/>
        <source>E&amp;xit</source>
        <translation>E&amp;sci</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="237"/>
        <source>&amp;About</source>
        <translation>Inform&amp;azioni</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="250"/>
        <source>Error Correction Level</source>
        <translation>Livello correzione degli errori</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="405"/>
        <source>Select data type:</source>
        <translation>Scegli il tipo di dati:</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="441"/>
        <source>characters count: %s - %d message(s)</source>
        <translation>conteggio caratteri: %s -%d messaggio</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="564"/>
        <source>ERROR: Something went wrong while trying to generate the QR Code.</source>
        <translation>ERRORE:qualcosa non ha funzionato durante la generazione del codice QR.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="609"/>
        <source>Save QRCode</source>
        <translation>Salva codice QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="602"/>
        <source>Save QR Code</source>
        <translation>Salva codice QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="602"/>
        <source>QR Code succesfully saved to %s</source>
        <translation>Codice QR salvato su %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="609"/>
        <source>QRCode succesfully saved to &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>Codice QR salvato su &lt;b&gt;%s&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="617"/>
        <source>Open QRCode</source>
        <translation>Apri codice QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="617"/>
        <source>Images (*.png *.jpg);; All Files (*.*)</source>
        <translation>Immagini (*.png *.jpg);; Tutti i file (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="636"/>
        <source>Decode File</source>
        <translation>Decodifica File</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="636"/>
        <source>No QRCode could be found in file: &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>Nessun codice QR trovato nel file &lt;b&gt;%s&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="666"/>
        <source>QRCode contains the following text:

%s</source>
        <translation>Il codice QR contiene il testo:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="667"/>
        <source>QRCode contains the following url address:

%s</source>
        <translation>Il codice QR contiene l&apos;url:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="668"/>
        <source>QRCode contains a bookmark:

Title: %s
URL: %s</source>
        <translation>Il codice QR contiene un segnalibro:

Titolo: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="669"/>
        <source>QRCode contains the following e-mail address:

%s</source>
        <translation>Il codice QR contiene l&apos;indirizzo email:

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="670"/>
        <source>QRCode contains an e-mail message:

To: %s
Subject: %s
Message: %s</source>
        <translation>Il codice QR contiene l&apos;email:

A: %s
Oggetto: %s
Corpo: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="671"/>
        <source>QRCode contains a telephone number: </source>
        <translation>Il codice QR contiene il numero di telefono: </translation>
    </message>
    <message>
        <location filename="qtqr.py" line="672"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s
Note: %s
Birthday: %s
Address: %s
URL: %s</source>
        <translation>Il codice QR contiene il contatto

Nome: %s
Tel: %s
E-Mail: %s
Note: %s
Compleanno: %s
Indirizzo: %s
URL: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="680"/>
        <source>QRCode contains the following SMS message:

To: %s
Message: %s</source>
        <translation>Il codice QR contiene un SMS

A: %s
Messaggio: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="681"/>
        <source>QRCode contains the following MMS message:

To: %s
Message: %s</source>
        <translation>Il codice QR contiene un MMS:

A: %s
Messaggio: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="682"/>
        <source>QRCode contains the following coordinates:

Latitude: %s
Longitude:%s</source>
        <translation>Il codice QR contiene le coordinate:

Latitudine: %s
Longitudine:%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="683"/>
        <source>QRCode contains the following WiFi Network Configuration:

SSID: %s
Encryption Type: %s
Password: %s</source>
        <translation>Il codice QR contiene la configurazione di una rete WiFi:

SSID: %s
Cifratura: %s
Password: %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="684"/>
        <source>QRCode contains the following Single Payment Information:

Name: %s
Account: %s
BNC: %s
Ammount: %s
Reason: %s
Currency: %s
</source>
        <translation>Il codice QR contiene indicazioni per un bonifico unico:

Nome: %s
Conto: %s
BNC: %s
Importo: %s
Causale: %s
Valuta: %s
</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="693"/>
        <source>

Do you want to </source>
        <translation>

Vuoi </translation>
    </message>
    <message>
        <location filename="qtqr.py" line="697"/>
        <source>open it in a browser?</source>
        <translation>aprirlo nel browser?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="698"/>
        <source>send an e-mail to the address?</source>
        <translation>mandare un&apos;E-Mail?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="699"/>
        <source>send the e-mail?</source>
        <translation>mandare l&apos;E-Mail?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="704"/>
        <source>open it in Google Maps?</source>
        <translation>aprirlo su Google Maps?</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="721"/>
        <source>Decode QRCode</source>
        <translation>Decodifica codice QR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>&amp;Edit</source>
        <translation>&amp;Modifica</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="824"/>
        <source>Decoding Failed</source>
        <translation>Decodifica fallita</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="824"/>
        <source>&lt;p&gt;Oops! no code was found.&lt;br /&gt; Maybe your webcam didn&apos;t focus.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Oops! Non è stato trovato nessun codice.&lt;br /&gt; Forse la webcam non mette a fuoco.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="835"/>
        <source>About QtQR</source>
        <translation>Informazioni su QtQR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="835"/>
        <source>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;A simple software for creating and decoding QR Codes that uses python-qrtools as backend. Both are part of the &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; project.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;This is Free Software: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Please visit our website for more information and to check out the code:&lt;br /&gt;&lt;a href=&quot;https://code.launchpad.net/qr-tools/&quot;&gt;https://code.launchpad.net/qr-tools/&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</source>
        <translation>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;Un semplice software per la codifica e la decodifica di codici QR basato su python-qrtools. Entrambi sono parte del progetto &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Questo programma è software libero: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Visita il sito web per maggiori informazioni e per consultare il codice:&lt;br /&gt;&lt;a href=&quot;https://code.launchpad.net/qr-tools/&quot;&gt;https://code.launchpad.net/qr-tools/&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;&lt;p&gt;traduzione italiana a cura di &lt;a href=&quot;mailto:giomba@linux.it&quot;&gt;giomba&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>VideoDevices</name>
    <message>
        <location filename="qtqr.py" line="879"/>
        <source>Decode from Webcam</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qtqr.py" line="884"/>
        <source>You are about to decode from your webcam. Please put the code in front of your camera with a good light source and keep it steady.
QtQR will try to detect automatically the QR Code.

Please select the video device you want to use for decoding:</source>
        <translation></translation>
    </message>
</context>
</TS>
